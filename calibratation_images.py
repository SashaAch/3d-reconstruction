import cv2 as cv

capL = cv.VideoCapture(2)
capR = cv.VideoCapture(1)
num = 0

while capL.isOpened():
    res1, imgL = capL.read()
    res2, imgR = capR.read()
    imgL = cv.flip(imgL, flipCode=0)
    imgR = cv.flip(imgR, flipCode=0)
    imgL = cv.flip(imgL, flipCode=1)
    imgR = cv.flip(imgR, flipCode=1)

    k = cv.waitKey(5)

    if k == 27:
        break
    elif k == ord('s'):
        cv.imwrite(f'images/left_images/imageL {num}.png', imgL)
        cv.imwrite(f'images/right_images/imageR {num}.png', imgR)
        print('Saved!')
        num += 1

    cv.imshow('ImageL', imgL)
    cv.imshow('ImageR', imgR)

capL.release()
capR.release()
